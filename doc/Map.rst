Map
=============

The map class contains the information about the game state

Functions
---------

const vector<vector<Building*>>& getBuildings()
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Function used to access the Buildings\_ vector

Parameters
----------

vector<vector<Building*>> Buildings\_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Vector containing the 2D grid of buildings in the game. Accessed via function getBuildings()

Map* map_
^^^^^^^^^

Contains link to map class 



