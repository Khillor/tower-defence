#include "mainWindow.hpp"
#include "map.hpp"

#include <chrono>
#include <ctime>
#include <thread>
#include <iostream>
#include <fstream>
#include <string>

#include <SFML/Window.hpp>

using namespace std;
using namespace sf;

mainWindow::mainWindow(){
 
    w_ = new RenderWindow(VideoMode(800,600),"Kakka ikkuna");
    
    map_ = new Map;
    run();
}

void mainWindow::run(){

    while(w_->isOpen()){

        Event e;
        while(w_->pollEvent(e)){
            if(e.type == Event::Closed){
                w_->close();
            }
            if(e.type == Event::KeyPressed){
                w_->close();
            }
        }
        draw();
        this_thread::sleep_for(chrono::milliseconds(100));
    }

    return;
}

void drawGrid(RenderWindow* w, pair<int,int> m){

    Vector2u s = w->getSize();

    RectangleShape recH = RectangleShape(Vector2f(s.x, 1));
    recH.setFillColor(Color::White);
    RectangleShape recV = RectangleShape(Vector2f(1,s.y));
    int dx = s.x/m.first;
    int dy = s.y/m.second;

    for(int i = 1; i < m.first; i++){
        recH.setPosition(0,i*dy);
        w->draw(recH);
    } 
    for(int i = 1; i< m.second; i++){
        recV.setPosition(i*dx,0);
        w->draw(recV);
    }
    return;
}

void drawBuildings(RenderWindow* w, Map* m){

    (void)w;
    (void)m;

    for(auto b_row : m->getBuildings()){
        for(auto b : b_row){
            //cout << b->getHP() << endl;
            //cout << b->getR()->getSize().x << endl;
            b->draw(w);
        }
    }

    return;
}

// Drawing function for all of the elements
void mainWindow::draw(){
    w_->clear(Color::Green); 
    drawGrid(w_, map_->getMapSize());
    drawBuildings(w_, map_);
    w_->display();

    return;
}




















// Don't look down
