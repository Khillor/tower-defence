#include "building.hpp"
#include "mainWindow.hpp"

#include <SFML/Window.hpp>

#include "map.hpp"

#include <iostream>

using namespace std;


void Building::draw(RenderWindow* w){
    pair<int,int> m =  map_->getMapSize();
    int dx = w->getSize().x/m.first;
    int dy = w->getSize().y/m.second;
    //r_->setSize(Vector2f(0.5*dx,0.5*dy));
    r_->setScale(0.8*dx/r_->getSize().x,0.8*dy/r_->getSize().y);
    
    r_->setPosition((Coords.first+0.5)*dx, (Coords.second+0.5)*dy);
    w->draw(*r_);
    

    return;
}

