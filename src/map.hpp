#ifndef MAP_H
#define MAP_H
#include <vector>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "building.hpp"

using namespace std;
using namespace sf;

class Map{

    public:
        Map(const int size = 10);
        ~Map(){}
        pair<int,int> getMapSize(){return make_pair(size_,size_);};
         const vector<vector<Building*>>& getBuildings(){return Buildings_;};

    private:
        int time; // Time variable
        int size_;

        vector<vector<Building*>> Buildings_; // Vector containing all 

};


#endif
