#ifndef WINDOW_H
#define WINDOW_H
#include <iostream>
#include <string>
#include <vector>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "map.hpp"


using namespace std;
using namespace sf;

class mainWindow{

    public:
        mainWindow();
        ~mainWindow(){}

        RenderWindow* getWindow(){return w_;};

    private:

        Map* map_;
        RenderWindow* w_;
        void run(); 
        void draw(); // Draw all elements in map_
        int FPS=1; // Refreshrate of the window

};

#endif 
