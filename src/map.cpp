#include "map.hpp"

#include <vector>
#include <iostream>

using namespace std;

Map::Map(const int size){
    
    time = 0;
    size_ = size;

    vector<Building*> bTemp;
    for(int i = 0; i < size; i++){
        bTemp.clear();
        for(int j = 0; j < size; j++){
            bTemp.push_back(new Building(i,j,this));
        }
        Buildings_.push_back(bTemp);
    }

}
