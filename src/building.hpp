#ifndef BUILD_H
#define BUILD_H

#include <vector>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Map;

using namespace std;
using namespace sf;

class Building{

    public:
        Building(int X, int Y, Map* m):Coords(make_pair(X,Y)), HP(10), map_(m){r_ = new RectangleShape(Vector2f(10,10));
        r_->setFillColor(Color::Blue);
        r_->setOrigin(5,5);}
        ~Building(){}
        void draw(RenderWindow* w);
        int getHP(){return HP;};
        RectangleShape* getR(){return r_;};

    private:
        pair<int,int> Coords;
        int HP;
        Map* map_;

        RectangleShape* r_;
};

class DefTurret : public Building{

    public:
        DefTurret(int X, int Y, Map* m) : Building(X,Y,m){}
        ~DefTurret(){}



};









#endif
