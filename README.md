# A TD/survival game in the spirit of They Are Billions

The idea is to create a central hub that needs to be defended and which is attacked by swarm of enemies coming from all around the map

## The player 
*Places turrets to stop enemies

### Turrets have
- Range
- Cost
- Armor/HP
- Targetting mechanism
- Damage

## Enemies have
- Armor/HP
- Drop
- Speed
- Damage
- Attack range
- Path find algorithm

## Implementation order

0. Working map
1. Placeable turrets
2. Moving enemies
3. Damage
4. Enviroment
5. Path finding algorithms
6. Economy/Resources
